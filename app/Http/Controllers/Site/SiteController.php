<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SiteController extends Controller
{
    public function index()
    {
        $data['title'] = 'Home';
        return view('site.home.index', $data);
    }

    public function promotions()
    {
        $data['title'] = 'Promoções';
        return view('site.promotions.index', $data);
    }
}
